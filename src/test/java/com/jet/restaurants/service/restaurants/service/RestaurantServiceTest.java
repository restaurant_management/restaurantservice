package com.jet.restaurants.service.restaurants.service;

import com.jet.restaurants.service.restaurants.domain.Restaurant;
import com.jet.restaurants.service.restaurants.domain.Status;
import com.jet.restaurants.service.restaurants.repo.RestaurantRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Optional;

public class RestaurantServiceTest {

    private RestaurantService restaurantService;

    @Mock
    private RestaurantRepository restaurantRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        restaurantService = new RestaurantService(restaurantRepository);
    }

    @DisplayName("Given a valid restaurant name and status open, when I update the status of this restaurant to closed, " +
            "then status of restaurant will be closed")
    @Test
    public void updateRestaurantStatus_givenValidRestaurantNameAndStatusOpen_setStatusAsClosed() {

        String restaurantName = "Burguer King";
        Mockito.when(restaurantRepository.findByName(restaurantName)).
                thenReturn(Optional.of(Restaurant.builder().name(restaurantName).status(Status.OPEN).build()));

        restaurantService.updateRestaurantStatus(restaurantName, Status.CLOSED);

        var argumentCaptor = ArgumentCaptor.forClass(Restaurant.class);
        Mockito.verify(restaurantRepository, Mockito.times(1)).save(argumentCaptor.capture());

        var restaurantSaved = argumentCaptor.getValue();

        Assertions.assertAll(() -> {
            Assertions.assertEquals(restaurantName, restaurantSaved.getName());
            Assertions.assertEquals(Status.CLOSED, restaurantSaved.getStatus());
        });

    }

    @DisplayName("Given an invalid restaurant name, when I try update to status closed, then the restaurant is not found and status can't be updated")
    @Test
    public void updateRestaurantStatus_givenInvalidRestaurantName_restaurantNotFound() {

        String invalidRestaurantName = "Burguer Kin";
        Mockito.when(restaurantRepository.findByName(invalidRestaurantName)).
                thenReturn(Optional.empty());

        restaurantService.updateRestaurantStatus(invalidRestaurantName, Status.CLOSED);

        Mockito.verify(restaurantRepository, Mockito.times(0)).save(Mockito.any(Restaurant.class));

    }

}
