package com.jet.restaurants.service.listener;

import com.jet.restaurants.service.restaurants.domain.Status;
import com.jet.restaurants.service.restaurants.listener.RestaurantStatusListener;
import com.jet.restaurants.service.restaurants.service.RestaurantService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class RestaurantStatusListenerTest {

    private RestaurantStatusListener restaurantStatusListener;

    @Mock
    private RestaurantService restaurantService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        restaurantStatusListener = new RestaurantStatusListener(restaurantService);
    }

    @DisplayName("Given a restaurant called Burger King with status CLOSED to be consumed, when I consume the information," +
            " then the restaurant and the new status will be updated")
    @Test
    public void consume_givenDataToBeConsumed_keyWillBeANameAndValueWillBeValidStatus() {
        final var restaurantName = "Burger King";
        final var restaurantStatus = "closed";
        ConsumerRecord<String, String> consumerRecord = new ConsumerRecord<>("topic_test", 0, 0, restaurantName, restaurantStatus);

        restaurantStatusListener.consume(consumerRecord);
        var nameCaptor = ArgumentCaptor.forClass(String.class);
        var statusCaptor = ArgumentCaptor.forClass(Status.class);

        Mockito.verify(restaurantService, Mockito.times(1))
                .updateRestaurantStatus(nameCaptor.capture(), statusCaptor.capture());

        Assertions.assertAll(() -> {
            Assertions.assertEquals(restaurantName, nameCaptor.getValue());
            Assertions.assertEquals(Status.CLOSED, statusCaptor.getValue());
        });
    }

}
