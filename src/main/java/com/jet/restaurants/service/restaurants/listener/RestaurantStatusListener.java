package com.jet.restaurants.service.restaurants.listener;

import com.jet.restaurants.service.restaurants.domain.Status;
import com.jet.restaurants.service.restaurants.service.RestaurantService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RestaurantStatusListener {

	@Value("${topic.name.consumer}")
	private String topicName;

	private final RestaurantService restaurantService;

	@Autowired
	public RestaurantStatusListener(RestaurantService restaurantService) {
		this.restaurantService = restaurantService;
	}

	@KafkaListener(topics = "${topic.name.consumer}", groupId = "restaurantService")
	public void consume(ConsumerRecord<String, String> payload){
		this.restaurantService.updateRestaurantStatus(payload.key(), Status.decode(payload.value()));
	}
}
