package com.jet.restaurants.service.restaurants.service;

import com.jet.restaurants.service.restaurants.domain.Restaurant;
import com.jet.restaurants.service.restaurants.domain.Status;
import com.jet.restaurants.service.restaurants.repo.RestaurantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RestaurantService {
    private final RestaurantRepository restaurantRepository;

    @Autowired
    public RestaurantService(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    public Restaurant createRestaurant(String name, String city, String addressLine1, String addressLine2, String zipCode, String country, Status status) {

        return restaurantRepository.save(Restaurant.create(name, city, addressLine1, addressLine2, zipCode, country, status));
    }

    public void updateRestaurantStatus(String restaurantName, Status status) {

        var restaurantOptional = this.restaurantRepository.findByName(restaurantName);

        if (restaurantOptional.isPresent()) {
            var restaurant = restaurantOptional.get();
            restaurant.setStatus(status);
            this.restaurantRepository.save(restaurant);
        } else {
            log.info("Restaurant not found {} ", restaurantName);
        }

    }

}
