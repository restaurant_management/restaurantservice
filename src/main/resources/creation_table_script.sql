-- public.restaurants definition

-- Drop table

-- DROP TABLE public.restaurants;

CREATE TABLE public.restaurants (
	id serial NOT NULL,
	name varchar NOT NULL,
	city varchar NOT NULL,
	address_line1 varchar NULL,
	address_line2 varchar NOT NULL,
	zip_code varchar NOT NULL,
	country varchar NOT NULL,
	status varchar NOT NULL,
	CONSTRAINT restaurants_pkey PRIMARY KEY (id)
);

CREATE UNIQUE INDEX restaurants_name_idx ON public.restaurants ("name");
